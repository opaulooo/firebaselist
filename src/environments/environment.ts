// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyAKdwDgWaiXcjpFR_F3YCZ8Vdxpz3EWog0",
    authDomain: "listfire-iftm.firebaseapp.com",
    databaseURL: "https://listfire-iftm-default-rtdb.firebaseio.com",
    projectId: "listfire-iftm",
    storageBucket: "listfire-iftm.appspot.com",
    messagingSenderId: "544411480754",
    appId: "1:544411480754:web:f2a291beae5b2a82280fab",
    measurementId: "G-J1HFFNYSYR"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
