import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { List } from './list';
import { NgForm } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  constructor(private firestore: AngularFirestore) { }

  post(data: NgForm) {
    return new Promise<any>((resolve, reject) => {
      this.firestore
        .collection("listfire")
        .add(data)
        .then(res => {
          resolve(res)
        }, err => {
          reject(err)
        });
    });
  }

  get() {
    return (this.firestore.collection("listfire").snapshotChanges());
  }

  delete(data: any) {
    return (this.firestore
      .collection("listfire")
      .doc(data.payload.doc.id)
      .delete());
  }
}
