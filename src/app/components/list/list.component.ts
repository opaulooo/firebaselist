import { Component } from '@angular/core';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';
import { List } from 'src/app/shared/services/list';
import { ListService } from 'src/app/shared/services/list.service';

@Component({
  selector: 'app-lists',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

  user = JSON.parse(localStorage.getItem('user')!);
  maxRows: number = 10;
  loading: boolean = true;
  listData: any;
  form: FormGroup = Object.create(null);


  constructor(private service: ListService,
    public authService: AuthService) {

    this.form = new FormGroup({
      'nome': new FormControl(null, [Validators.required]),
      'idade': new FormControl(null, [Validators.required]),
      'altura': new FormControl(null, [Validators.required]),
      'peso': new FormControl(null, [Validators.required]),
    }, { updateOn: 'change' });


  }

  async ngOnInit() {

    this.listData = await this.get();
    console.log(this.listData)
  }


  async get() {
    return new Promise((resolve, reject) => {
      this.service.get().subscribe((res: any) => {
        let data: any[] = [];

        console.log(this.user.email)

        res.forEach((element: any) => {

          let nome = element.payload._delegate.doc._document.data.value.mapValue.fields.nome.stringValue;
          let idade = element.payload._delegate.doc._document.data.value.mapValue.fields.idade.stringValue;
          let peso = parseFloat(element.payload._delegate.doc._document.data.value.mapValue.fields.peso.stringValue);
          let altura = parseFloat(element.payload._delegate.doc._document.data.value.mapValue.fields.altura.stringValue);

          let imc = '';
          let imcNumber = parseFloat(((peso) / (altura * altura) * 10000).toFixed(2));

          if (imcNumber < 18.5) {
            imc = '(' + imcNumber + ') - Magreza';
          }
          else if (18.5 >= imcNumber && imcNumber <= 24.9) {
            imc = '(' + imcNumber + ') - Normal';
          }
          else if (25 >= imcNumber && imcNumber <= 29.9) {
            imc = '(' + imcNumber + ') - Sobrepeso';
          }
          else if (30 >= imcNumber && imcNumber <= 39.9) {
            imc = '(' + imcNumber + ') - Obesidade';
          }
          else if (imcNumber >= 40) {
            imc = '(' + imcNumber + ') - Obesidade Grave';
          }


          let object = {
            nome: nome,
            idade: idade,
            peso: peso,
            altura: altura,
            imc: imc,
          }
          data.push(object)
        });
        resolve(data)
      })
    });
  }

  addPessoa(row: NgForm) {
    console.log(row)

    this.service.post(row)
      .then(res => {
        console.log(res)
        this.ngOnInit();
      }, err => {
        console.log(err)
      });
  }

  deleteOrder(data: any) {
    this.service.delete(data).then(() => {

    }, err => {
      console.log(err)
    });
  }

  signout() {
    console.log('exit')
  }


}
