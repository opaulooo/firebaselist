# Firebaselist

Esse projeto foi desenvolvido pelos [autores](https://gitlab.com/opaulooo/firebaselist/-/blob/main/AUTHORS.md) com o [Angular CLI](https://github.com/angular/angular-cli) na versão 13.1.4.

# Rodando

Primeiramente, configure o acesso ao seu projeto do firebase no arquivo `src/environments/environment.ts`. Certifique-se de possuir o Angular instalado e rode o comando `npm install` na pasta raiz do projeto.

Após isso, execute `ng serve --open` para iniciar a aplicação.

# Como funciona

A primeira página da aplicação solicita um login, que pode ser feito por email e senha ou utilizandoo google. Caso você não possua uma conta ainda, clique em "Registre-se" para criar sua conta.

Com uma conta criada, faça o login e você terá acessa à lista para ver o cadastro dos pacientes, onde pode informar idade, altura e peso. Após a inserção desses dados, o sistema irá salvar no banco de dados do firebase e irá apresentar as informações realizando o cálculo do IMC de cada paciente.

